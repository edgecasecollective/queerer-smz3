import React from 'react';
import { Jumbotron, Container, Row, Col, Button } from 'reactstrap';

export default function Smz3Home() {
    return (
        <>
            <Jumbotron>
                <Container>
                    <h1 className="display-4">A Slightly Queerer</h1>
                    <h1 style={{ textAlign: "right" }} className="display-4">SMZ3 Crossover Randomizer</h1>
                    <h5>Welcome!</h5>
                    <p>
                        This is a small fork of the smz3 randomizer by Total. It is intended, for the moment, as a temporary home for queer sprites while that community
                        works out its new rules around sprite approvals. It may also in the future be a home for other experiments in randomizer development!
                    </p>
                    <p>
                        This deployment of the randomizer was set up by The Edge Case Collective, a twitch streaming group of creative queers playing, building, and 
                        breaking games in interesting ways! hacks, randos, music, analysis through a queer lens &amp; more! If you're interested in what we're doing here,
                        give us a follow over at <a href="https://twitch.tv/edgecasecollective">our twitch page</a> or <a href="https://twitter.com/edgecasegaming">on twitter</a>!
                    </p>
                    <p>
                        For the original randomizer, which may be more up to date depending on when you look at this, please go to <a href="https://samus.link">samus.link</a>!
                    </p>
                </Container>
            </Jumbotron>
            <Container>
                <Row>
                    <Col md="4">
                        <h2>Get started</h2>
                        <p>Follow the link below to get to the game generation page and head directly into the action.</p>
                        <span className="align-bottom"><a href="/configure"><Button color="primary">Generate game</Button></a></span>
                    </Col>
                    <Col md="4">
                        <h2>Get help</h2>
                        <p>If this is your first time playing or you're looking for more information about the randomizer go here:</p>
                        <span className="align-bottom"><a href="https://samus.link/information"><Button color="primary">Information</Button></a></span>
                    </Col>
                    <Col md="4">
                        <h2>Get involved</h2>
                        <p>If you're looking to get involved with the randomizer community, take a look at the resources for more information.</p>
                        <span className="align-bottom"><a href="https://samus.link/resources"><Button color="primary">Resources</Button></a></span>
                    </Col>
                </Row>
                {/* <Row style={{ marginTop: "30px" }}>
                    <Col md={{ size: 4, offset: 8 }}>
                        <h2>Donate</h2>
                        <p>Donations to help out with costs for keeping the randomizer running for everyone is greatly appreciated.</p>
                        <span className="align-bottom">
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                <input type="hidden" name="cmd" value="_s-xclick" />
                                <input type="hidden" name="hosted_button_id" value="TD6E7WSKHXFA2" />
                                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
                                <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
                            </form>
                        </span>
                    </Col>
                </Row> */}
            </Container>
        </>
    );
}
